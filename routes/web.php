<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware'=>[],'prefix'=>'api/v1'],function() use ($router){
    $router->post('/users/login','UsersController@getToken');
} );
$router->group(['middleware'=>['auth'],'prefix'=>'api/v1' ],function() use ($router){
    $router->get('/users','UsersController@index');
    $router->post('/users','UsersController@createUser');
    $router->put('/users/{id}','UsersController@updateUser');
    $router->delete('/users/{id}','UsersController@deleteUser');
});
